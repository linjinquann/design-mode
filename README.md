1. template 模板模式
2. facde 外观/门面模式


3. pom.xml
		<!-- logback begin -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>1.7.7</version>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.1.2</version>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-core</artifactId>
			<version>1.1.2</version>
		</dependency>
		<dependency>
			<groupId>org.logback-extensions</groupId>
			<artifactId>logback-ext-spring</artifactId>
			<version>0.1.1</version>
		</dependency>
		<!-- logback end -->
		<!--spring base begin-->
			spring-core
			spring-beans
			spring-context-support
			spring-test
		<!--spring base end-->
		
4.logback.xml
	<root>
		<level value="debug" />
		 <appender-ref ref="stdout" />   //在console显示,不需要注释
		<appender-ref ref="logfile" />
	</root>
	
	
5.
com.ljq.design.adapter 适配器模式
	解决类的兼容问题。
		类适配器：继承父类，实现新接口，来扩展新子类的功能
		接口适配器：接口有很多功能，新类只想实现部分功能，在中间增加隔离层抽象类，实现全部。
		对象适配器：持有源对象引用，进行源对象方法调用，实现新接口，来扩展新类的功能
		
com.ljq.design.bridge 桥接
	目的：将抽象化与实现化解耦
		抽象类持有接口的引用，这样可以不断的扩展抽象的子类功能，和接口的功能。
		
com.ljq.design.command 命令模式
	invoice持有command(持有receive)--invoice发命令
	
com.ljq.design.composite 组合
	组合模式的使用场景就是出现树形结构的地方
	
com.ljq.design.cor 责任链模式（Chain of Responsibility）
	解决如oa审批，上级处理不了，交给更上级处理
	
com.ljq.design.decorator 装饰器
	解决：给同一接口类增加功能，和被装饰者实现同一接口/父类，并且持有该接口/父类（即被装饰者引用）
		动态、透明地给特定对象添加职责/撤销职责，也就是说，不会影响到其他对象 
		
com.ljq.design.facde 门面/外观
	解决：一个应用有很多细小的功能，用外观封装一个更大的功能给调用者。如一个电视。打开电视按钮方法（外观）--封装方法（通电方法，获取关机前的台号方法,等）
	
com.ljq.design.factory 工厂
com.ljq.design.flyweight 享元模式
	如连接池，维护这个池
	
com.ljq.design.interpreter 解释器模式
com.ljq.design.iterator  迭代器模式
com.ljq.design.mediator 中介者模式，中介调度
com.ljq.design.memento  备忘录
	备份恢复

com.ljq.design.observer 观察者
	持有观察者引用通知

com.ljq.design.proxy 代理模式
	给对象增加新功能。
	
com.ljq.design.state 状态模式
com.ljq.design.strategy 策略模式
com.ljq.design.template 模板模式
	同一种行为，让子类重写
com.ljq.design.visitor  访问者模式

6.归纳
参考：https://www.cnblogs.com/maowang1991/archive/2013/04/15/3023236.html
创建型模式，共五种：工厂方法模式、抽象工厂模式、单例模式、建造者模式、原型模式。
结构型模式，共七种：适配器模式、装饰器模式、代理模式、外观模式、桥接模式、组合模式、享元模式。
行为型模式，共十一种：策略模式、模板方法模式、观察者模式、迭代子模式、责任链模式、命令模式、备忘录模式、状态模式、访问者模式、中介者模式、解释器模式。

