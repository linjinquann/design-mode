package com.ljq.design.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class State {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void method1() {
		logger.info("execute the first opt!");
	}

	public void method2() {
		logger.info("execute the second opt!");
	}
}
