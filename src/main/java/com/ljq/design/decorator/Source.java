package com.ljq.design.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Source implements ISource{

	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void say() {
		logger.info("Source.say()");
		
	}

}
