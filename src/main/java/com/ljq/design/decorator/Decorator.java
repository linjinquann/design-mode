package com.ljq.design.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 装饰器模式的应用场景：
	1、需要扩展一个类的功能。
	2、动态的为一个对象增加功能，而且还能动态撤销。（继承不能做到这一点，继承的功能是静态的，不能动态增删。）
	3、装饰者在运行时构造
	缺点：产生过多相似的对象，不易排错！
 * 
 *         CreateDate: 2017年11月30日
 */
public class Decorator implements ISource {

	Logger logger = LoggerFactory.getLogger(getClass());
	
	private ISource source;  
    
    public Decorator(ISource source){  
        super();  
        this.source = source;  
    }
    
	@Override
	public void say() {
		before();
        source.say();
        after();
		
	}
	

	private void after() {
		logger.info("decorator.say() after");
		
	}

	private void before() {
		logger.info("decorator.say() before");
		
	}

}
