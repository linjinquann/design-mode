package com.ljq.design.mediator;

/**
 * 用户持有中介的引用
 *         CreateDate: 2017年12月2日
 */
public abstract class User {
	private Mediator mediator;

	public Mediator getMediator() {
		return mediator;
	}

	public User(Mediator mediator) {
		this.mediator = mediator;
	}

	public abstract void work();
}