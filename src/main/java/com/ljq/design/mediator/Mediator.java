package com.ljq.design.mediator;

/**
 * 
 * 中介接口
 *         CreateDate: 2017年12月2日
 */
public interface Mediator {
	
	//创建中介
	public void createMediator();

	public void workAll();

}
