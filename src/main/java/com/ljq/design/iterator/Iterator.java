package com.ljq.design.iterator;

/**
 * 迭代器模式就是顺序访问聚集中的对象
 * 一是需要遍历的对象，即聚集对象，
 * 二是迭代器对象，用于对聚集对象进行遍历访问。
 * 
 *         CreateDate: 2017年12月1日
 */
public interface Iterator {
	// 前移
	public Object previous();

	// 后移
	public Object next();

	public boolean hasNext();

	// 取得第一个元素
	public Object first();
}
