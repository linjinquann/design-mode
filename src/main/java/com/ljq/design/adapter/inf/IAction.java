package com.ljq.design.adapter.inf;

/**
 * 
 * 类说明：
 * 
 *         CreateDate: 2017年11月29日
 */
public interface IAction {
	public void say();
	public void fly();
}
