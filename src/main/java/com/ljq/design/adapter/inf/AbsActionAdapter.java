package com.ljq.design.adapter.inf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 接口的适配器模式
 * 类说明：
 * 	有时我们写的一个接口中有多个抽象方法，当我们写该接口的实现类时，必须实现该接口的所有方法，
 * 	因为并不是所有的方法都是我们需要的，有时只需要某一些，此处为了解决这个问题，
 * 	我们引入了接口的适配器模式，借助于一个抽象类，该抽象类实现了该接口，实现了所有的方法。
 * 而我们不和原始的接口打交道，只和该抽象类取得联系，所以我们写一个类，继承该抽象类，重写我们需要的方法就行。
 * 
 *         CreateDate: 2017年11月30日
 */
public abstract class AbsActionAdapter implements IAction {
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void say() {
		logger.info("AbsActionTargetAdapter.say()");
		
	}

	@Override
	public void fly() {
		logger.info("AbsActionTargetAdapter.fly()");
		
	}

}
