package com.ljq.design.adapter.cls;

/**
 * 类的适配器模式2-目標接口
 * 类说明：
 * 
 *         CreateDate: 2017年11月29日
 */
public interface IActionTarget {
	public void say();
	public void fly();
}
