package com.ljq.design.adapter.cls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 类的适配器模式3-適配器

 * CreateDate: 2017年11月29日
 */
@Component
public class BirdAdapter extends AnimalSource implements IActionTarget {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void fly() {
		logger.info("fly()");
	}
}
