package com.ljq.design.adapter.cls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 类的适配器模式1--源类
 * 类说明：
 * 
 *         CreateDate: 2017年11月29日
 */
public class AnimalSource {
	Logger logger = LoggerFactory.getLogger(getClass());
	public void say() {
		logger.info("say()");
	}
}
