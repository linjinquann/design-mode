package com.ljq.design.adapter.obj;

/**
 * 类说明：
 * 
 *         CreateDate: 2017年11月29日
 */
public interface IObjAction {
	public void say();
	public void fly();
}
