package com.ljq.design.adapter.obj;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 对象适配器模式3-適配器

 * CreateDate: 2017年11月29日
 */
@Component
public class FishAdapter implements IObjAction {
	Logger logger = LoggerFactory.getLogger(getClass());

	//可以让spring管理或者自己new 
	@Resource
	private AnimalSource animalSource;
	
	public AnimalSource getAnimalSource() {
		return animalSource;
	}

	public void setAnimalSource(AnimalSource animalSource) {
		this.animalSource = animalSource;
	}

	@Override
	public void say() {
		getAnimalSource().say();
	}
	
	@Override
	public void fly() {
		logger.info("fly()");
	}

	
}
