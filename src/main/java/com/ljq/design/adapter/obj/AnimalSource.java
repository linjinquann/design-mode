package com.ljq.design.adapter.obj;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 对象适配器模式1--源类
 * 类说明：
 * 
 *         CreateDate: 2017年11月29日
 */	
@Component
public class AnimalSource {
	Logger logger = LoggerFactory.getLogger(getClass());
	public void say() {
		logger.info("say()");
	}
}
