package com.ljq.design.bridge;

import org.springframework.stereotype.Component;

@Component
public class MyBridge extends AbsBridge {

	@Override
	public void driver() {
		getSourceDriver().driver();
	}
}
