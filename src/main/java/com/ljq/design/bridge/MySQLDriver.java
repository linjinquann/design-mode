package com.ljq.design.bridge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MySQLDriver implements ISourceDriver {
	Logger logger = LoggerFactory.getLogger(getClass());
	
	public void driver() {
		logger.info("MySQLDriver.driver()");
	}
}
