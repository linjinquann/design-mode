package com.ljq.design.observer;

public class MyObsSubject extends AbsObsSubject{
	
	@Override
	public void operation() {
		notifyObserver();
	}
}
