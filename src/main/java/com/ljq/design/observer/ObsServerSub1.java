package com.ljq.design.observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObsServerSub1 implements IObsServer {

	Logger logger = LoggerFactory.getLogger(getClass());
	@Override
	public void receive() {
		logger.info("ObserverSub1.receive()");
	}

}
