package com.ljq.design.observer;

import java.util.Iterator;
import java.util.Vector;

public class AbsObsSubject implements IObsSubject {

	private Vector<IObsServer> observers = new Vector<IObsServer>();
	
	@Override
	public void add(IObsServer observer) {
		observers.add(observer);
	}

	@Override
	public void del(IObsServer observer) {
		observers.remove(observer);
	}

	@Override
	public void notifyObserver() {
		Iterator<IObsServer> iterator = observers.iterator();
		while (iterator.hasNext()) {
			IObsServer observer = iterator.next();
			observer.receive();
		}
	}

	@Override
	public void operation() {
		
	}
}
