package com.ljq.design.observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObsServerSub2 implements IObsServer {

	Logger logger = LoggerFactory.getLogger(getClass());
	@Override
	public void receive() {
		logger.info("ObserverSub2.receive()");
	}

}
