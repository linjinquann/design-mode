package com.ljq.design.observer;

public interface IObsServer {
	void receive();
}
