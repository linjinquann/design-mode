package com.ljq.design.template;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ljq.design.dto.OrderInfoDto;


@Service
public class TemplateImpl {
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
	Template absTemplate;
	
	public Object cache(OrderInfoDto orderInfoDto) {
		logger.info("TemplateInfImpl.cache(OrderInfoDto orderInfoDto={}", orderInfoDto);
		Object rtObject = absTemplate.cache("key", orderInfoDto, new ITemplate() {
			@Override
			public Object doQuery(Object obj) {
				//处理相关对象
				return obj;
			}
		});
		return rtObject;
	}
}
