package com.ljq.design.template;

import org.springframework.stereotype.Component;

@Component
public class Template {
	

    public Object cache(String key, Object obj, ITemplate template){
        Object rtObject = template.doQuery(obj);;
    	
    	//缓存
        if(rtObject==null) {
        	
        	beforeCache(obj);
        	doCache(obj);
        	afterCache(obj);
        	rtObject = obj;
        }
        
        return rtObject;
    }

	private void beforeCache(Object obj) {
		// TODO Auto-generated method stub
		
	}

	private void afterCache(Object obj) {
		// TODO Auto-generated method stub
		
	}

	private void doCache(Object obj) {
		
	}


    
   
}
