package com.ljq.design.dto;

import java.io.Serializable;

public class OrderInfoDto implements Serializable { 


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 订单CODE .
     */
    private String orderCode;

    /**
     * 订单号 .
     */
    private String orderNo;

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Override
	public String toString() {
		return "OrderInfoDto [orderCode=" + orderCode + ", orderNo=" + orderNo + "]";
	}

	
	
}
