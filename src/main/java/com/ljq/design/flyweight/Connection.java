package com.ljq.design.flyweight;

/**
 *         CreateDate: 2017年12月1日 
 */
public class Connection {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public Connection() {
	}
	public Connection(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Connection [name=" + name + "]";
	}
	
	
}
