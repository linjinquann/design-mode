package com.ljq.design.visitor;

public interface Visitor {
	 public void visit(Subject sub);  
}
