package com.ljq.design.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PSource implements IPSource {

	Logger logger = LoggerFactory.getLogger(getClass());
	@Override
	public void say() {
		logger.info("PSource.say()");
	}

}
