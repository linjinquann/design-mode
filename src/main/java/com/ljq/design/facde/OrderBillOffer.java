package com.ljq.design.facde;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ljq.design.dto.OrderInfoDto;
import com.ljq.design.facde.impl.OrderBillServiceImpl;


@Component
public class OrderBillOffer extends OrderBillAbs {
	@Autowired
	OrderBillServiceImpl orderBillServiceImpl;
	
	@Override
	public boolean before(OrderInfoDto orderInfoDto) {
		return super.before(orderInfoDto);
	}

	@Override
	public void post() {
		super.post();
		logger.info("<<<<< OrderBillOffer post >>>>>");
		orderBillServiceImpl.upd(getOrderInfoDto());
	}

	@Override
	public void after() {
		super.after();
	}
	
	

}
