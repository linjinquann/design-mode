package com.ljq.design.facde;

import com.ljq.design.dto.OrderInfoDto;

public interface IOrderBillFacde {

	OrderInfoDto createOrderBill(OrderInfoDto orderInfoDto);
	
	OrderInfoDto offerOrderBill(OrderInfoDto orderInfoDto);
}
