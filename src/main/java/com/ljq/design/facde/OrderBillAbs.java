package com.ljq.design.facde;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ljq.design.dto.OrderInfoDto;


public abstract class OrderBillAbs {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	OrderInfoDto orderInfoDto;
	
	public OrderInfoDto getOrderInfoDto() {
		return orderInfoDto;
	}

	private void setOrderInfoDto(OrderInfoDto orderInfoDto) {
		this.orderInfoDto = orderInfoDto;
	}

	public boolean before(OrderInfoDto orderInfoDto) {
		logger.info("before={}", orderInfoDto);
		setOrderInfoDto(orderInfoDto);
		return true;
	}
	
	public void post() {
		logger.info("post={}", orderInfoDto);
	}
	
	public void after() {
		
	}
}
