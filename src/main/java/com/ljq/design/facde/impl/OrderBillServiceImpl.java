package com.ljq.design.facde.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ljq.design.dto.OrderInfoDto;
import com.ljq.design.facde.IOrderBillService;

@Service
public class OrderBillServiceImpl implements IOrderBillService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	public void add(OrderInfoDto orderInfoDto) {
		logger.info("OrderBillServiceImpl.add={}", orderInfoDto);
	}
	
	public void upd(OrderInfoDto orderInfoDto) {
		logger.info("OrderBillServiceImpl.upd={}", orderInfoDto);
	}
}
