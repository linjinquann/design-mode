package com.ljq.design.facde.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ljq.design.dto.OrderInfoDto;
import com.ljq.design.facde.IOrderBillFacde;
import com.ljq.design.facde.OrderBillAdd;
import com.ljq.design.facde.OrderBillOffer;

@Service
public class OrderBillFacdeImpl implements IOrderBillFacde {
	
	@Resource
	private OrderBillAdd orderBillAdd;
	@Resource
	private OrderBillOffer orderBillOffer;

	@Override
	public OrderInfoDto createOrderBill(OrderInfoDto orderInfoDto) {
		orderBillAdd.before(orderInfoDto);
		orderBillAdd.post();
		orderBillAdd.after();
		return null;
	}

	@Override
	public OrderInfoDto offerOrderBill(OrderInfoDto orderInfoDto) {
		orderBillOffer.before(orderInfoDto);
		orderBillOffer.post();
		orderBillOffer.after();
		return null;
	}

}
