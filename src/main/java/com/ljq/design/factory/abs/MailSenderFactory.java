package com.ljq.design.factory.abs;

import org.springframework.stereotype.Component;

import com.ljq.design.factory.ISender;
import com.ljq.design.factory.MailSender;

@Component
public class MailSenderFactory implements ISenderFactory {

	@Override
	public ISender produce() {
		logger.info("MailSenderFactory.produce()");
		return new MailSender();
	}
}
