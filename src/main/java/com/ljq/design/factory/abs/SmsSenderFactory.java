package com.ljq.design.factory.abs;

import org.springframework.stereotype.Component;

import com.ljq.design.factory.ISender;
import com.ljq.design.factory.SmsSender;

@Component
public class SmsSenderFactory implements ISenderFactory {

	@Override
	public ISender produce() {
		logger.info("SmsSenderFactory.produce()");
		return new SmsSender();
	}
}
