package com.ljq.design.factory.abs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ljq.design.factory.ISender;

/**
 * 抽象工厂，有新的产品继续这个
 * 
 *         CreateDate: 2017年11月29日
 */
public interface ISenderFactory {
	Logger logger = LoggerFactory.getLogger(ISender.class);
	ISender produce();
}
