package com.ljq.design.factory;

import org.springframework.stereotype.Component;

@Component
public class MailSender implements ISender{

	@Override
	public void Send() {
		 logger.info("this is mailsender!");
	}

}
