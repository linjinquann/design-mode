package com.ljq.design.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface ISender {
	Logger logger = LoggerFactory.getLogger(ISender.class);
	public void Send();  
}
