package com.ljq.design.factory.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ljq.design.factory.ISender;
import com.ljq.design.factory.MailSender;
import com.ljq.design.factory.SmsSender;
import com.ljq.design.util.SpringContextHolder;

/**
 * 普通工厂
 * 类说明：
 * @author 林进权
 * 
 *         CreateDate: 2017年11月29日
 */
@Component
public class SendFactoryGeneral {
	
	private Logger logger = LoggerFactory.getLogger(SendFactoryGeneral.class);
	
	public ISender getSender(Class clzz) {
		logger.info("getSender(Class clzz) begin={}", clzz.getName());
		ISender sender = null;
		if(clzz.getName().equals("com.ljq.design.factory.MailSender") ) {
			sender =  SpringContextHolder.getBean(MailSender.class);
		} else if (clzz.getName().equals("com.ljq.design.factory.SmsSender") ) {
			sender =  SpringContextHolder.getBean(SmsSender.class);
		}
		logger.info("getSender(Class clzz) end={}", clzz.getName());
		return sender;
	}
	
}
