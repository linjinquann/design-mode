package com.ljq.design.factory.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ljq.design.factory.ISender;
import com.ljq.design.factory.MailSender;
import com.ljq.design.factory.SmsSender;
import com.ljq.design.util.SpringContextHolder;

/**
 * 静态工厂
 * 类说明：
 * @author 林进权
 * 
 *         CreateDate: 2017年11月29日
 */
@Component
public class SendFactoryStatic {
	
	private static Logger logger = LoggerFactory.getLogger(SendFactoryStatic.class);
	
	public static ISender productMailSender() {
		logger.info("SendFactoryStatic.productMailSender()");
		return SpringContextHolder.getBean(MailSender.class);
	}
	
	
	public static ISender productSmsSender() {
		logger.info("SendFactoryStatic.productSmsSender()");
		return SpringContextHolder.getBean(SmsSender.class);
	}
}
