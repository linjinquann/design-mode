package com.ljq.design.factory.builder;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ljq.design.factory.ISender;
import com.ljq.design.factory.MailSender;

/**
 * 建造者模式（Builder）
 * 工厂类模式提供的是创建单个类的模式，
 * 而建造者模式则是将各种产品集中起来进行管理，用来创建复合对象，
 * 所谓复合对象就是指某个类具有不同的属性。
 * 
 * 工厂模式关注的是创建单个产品，而建造者模式则关注创建符合对象，多个部分
 * 
 *         CreateDate: 2017年11月29日
 */
@Component
public class SenderBuilder {
	Logger logger = LoggerFactory.getLogger(getClass());
	private List<ISender> list = new ArrayList<ISender>();  
	public List<ISender> produceMailSender(int count) {
		logger.info("SenderBuilder.produceMailSender(count)={}", count);
		for (int i = 0; i < count; i++) {
			list.add(new MailSender());
		}
		return list;
	}
}
