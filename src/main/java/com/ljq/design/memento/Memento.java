package com.ljq.design.memento;

public class Memento {

	private String value;
	private int hp;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public Memento(String value, int hp) {
		this.value = value;
		this.hp = hp;
	}

}
