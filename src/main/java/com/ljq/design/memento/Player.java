package com.ljq.design.memento;

public class Player {

	// 需要备分到备忘录的值
	private String value;
	private int hp;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public Memento createMemento() {
		return new Memento(value, hp);
	}

	public void resetValue(Memento memento) {
		this.value = memento.getValue();
		this.hp = memento.getHp();
	}

	@Override
	public String toString() {
		return "Player [value=" + value + ", hp=" + hp + "]";
	}

	
	
}
