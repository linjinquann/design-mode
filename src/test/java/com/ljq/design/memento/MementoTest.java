package com.ljq.design.memento;

import java.util.Random;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 备忘录模式，更名叫“备份-恢复”更合适
 * 类说明：
 * 
 * <p>
 * 
 * @Company: 领居科技有限公司
 * @author 林进权
 * 
 *         CreateDate: 2017年12月1日
 */
public class MementoTest {
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void mementoTest() {

		//创建玩家对象
		Player original = new Player();
		original.setValue("haha");
		original.setHp(100);
		
		//打boss前备份当前记录, 创建备忘录
		Memento memento = original.createMemento();
		
		//保存到硬盘里
		Storage storage = new Storage(memento);
		
		//开始打boss
		logger.info("打boss前={}", original);
		original.setHp(new Random().nextInt(100));
		logger.info("与boss切磋后={}", original);

		//如果还剩半条命，进行恢复
		if(original.getHp()<50) {
			//从备忘录里恢复
			original.resetValue(storage.getMemento());
			logger.info("被boss打暴，进行重置={}", original);
			
		} else {
			logger.info("顺利过关={}", original);
			
		}
		
		
		
	}
}
