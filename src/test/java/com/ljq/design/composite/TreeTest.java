package com.ljq.design.composite;

import org.junit.Test;

/**
 * 组合模式有时又叫部分-整体模式在处理类似树形结构的问题时比较方便，看看关系图：
 * 
 *         CreateDate: 2017年12月1日
 */
public class TreeTest {
	@Test
	public void treeTest() {
		Tree tree = new Tree("A");
		TreeNode nodeB = new TreeNode("B");
		TreeNode nodeC = new TreeNode("C");

		nodeB.add(nodeC);
		tree.root.add(nodeB);
		System.out.println("build the tree finished!");
	}
}
