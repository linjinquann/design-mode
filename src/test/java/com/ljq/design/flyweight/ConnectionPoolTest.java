package com.ljq.design.flyweight;

import org.junit.Test;

/**
 * 享元模式的主要目的是实现对象的共享，即共享池，当系统中对象多的时候可以减少内存的开销，通常与工厂模式一起使用。
 * 像jdbc连接池
 * 
 *         CreateDate: 2017年12月1日
 */
public class ConnectionPoolTest {
	
	@Test
	public void getConnectionFail() {
		int pool = 10;
		ConnectionPool connectionPool = ConnectionPool.init();
		for (int i = 0; i < pool+1; i++) {
			connectionPool.getConnection();
		}
	}
	
	@Test
	public void getConnectionSucess() {
		int pool = 10;
		ConnectionPool connectionPool = ConnectionPool.init();
		for (int i = 0; i < pool+1; i++) {
			Connection connection = connectionPool.getConnection();
			connectionPool.release(connection);
		}
	}
}
