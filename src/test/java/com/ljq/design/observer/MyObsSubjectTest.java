package com.ljq.design.observer;

import org.junit.Test;

/**
 * 
观察者模式很好理解，类似于邮件订阅和RSS订阅，当你订阅了该文章，如果后续有更新，会及时通知你。
当一个对象变化时，其它依赖该对象的对象都会收到通知，并且随着变化！对象之间是一种一对多的关系
 * 
 *         CreateDate: 2017年12月1日
 */
public class MyObsSubjectTest{
	
	@Test
	public void operationTest() {
		IObsServer sub1 = new ObsServerSub1();
		IObsServer sub2 = new ObsServerSub2();
		
		IObsSubject mySubject = new MyObsSubject();
		mySubject.add(sub1);
		mySubject.add(sub2);
		mySubject.notifyObserver();
		
	}
}
