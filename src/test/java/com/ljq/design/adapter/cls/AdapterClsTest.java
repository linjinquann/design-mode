package com.ljq.design.adapter.cls;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 类适配器模式
 * 类说明：
 * 
 *         CreateDate: 2017年11月29日
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value="classpath:application.xml")
public class AdapterClsTest {
	
	@Resource
	IActionTarget actionTarget;
	
	@Test
	public void testAdapter() {
		actionTarget.say();
		actionTarget.fly();
	}
}
