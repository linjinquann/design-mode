package com.ljq.design.proxy;

import org.junit.Test;

/**
 * 装饰器模式关注于在一个对象上动态的添加方法，然而代理模式关注于控制对对象的访问。
 * 用代理模式，代理类（proxy class）可以对它的客户隐藏一个对象的具体信息。
 * 因此，当使用代理模式的时候，我们常常在一个代理类中创建一个对象的实例， 关系在编译时确定。
 * 并且，当我们使用装饰器模 式的时候，我们通常的做法是将原始对象作为一个参数传给装饰者的构造器，装饰者能够在运行时被构造。
 * 类说明：
 * 
 * <p>
 *         CreateDate: 2017年11月30日
 */
public class ProxyTest {

	@Test
	public void testDecorator() {
		IPSource source = new Proxy();
		source.say();
	}
}
