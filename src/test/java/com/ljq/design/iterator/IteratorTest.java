package com.ljq.design.iterator;

import org.junit.Test;

/**
 * 迭代器模式就是顺序访问聚集中的对象
 * 一是需要遍历的对象，即聚集对象，
 * 二是迭代器对象，用于对聚集对象进行遍历访问。
 * 
 *         CreateDate: 2017年12月1日
 */
public class IteratorTest {

	@Test
	public void iteratorTest() {
		Collection collection = new MyCollection();
		Iterator it = collection.iterator();

		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}
}
