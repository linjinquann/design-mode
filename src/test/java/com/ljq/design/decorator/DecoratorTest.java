package com.ljq.design.decorator;

import org.junit.Test;


/**
 * 装饰器模式的应用场景：
	1、需要扩展一个类的功能。
	2、动态的为一个对象增加功能，而且还能动态撤销。（继承不能做到这一点，继承的功能是静态的，不能动态增删。）
	3、装饰者在运行时构造
	缺点：产生过多相似的对象，不易排错！
 * 
 *         CreateDate: 2017年11月30日
 */
public class DecoratorTest {

	@Test
	public void testDecorator() {
		//客户指定了装饰者需要装饰的是哪一个类
		ISource source = new Source();  
		ISource obj = new Decorator(source);  
        obj.say();  
	}
}

/**
假设这样一种场景，我们要为类Origin增加A功能，B功能和C功能。

JDK动态代理：需要生成三个Origin的Handler来分别增加A,B,C功能。
这三个代理类分别为OriginHandlerA,OriginHandlerB,OriginHandlerC

装饰器模式：也需要生成三个Origin的装饰器来增加A,B,C的功能。
这三个装饰器分别为OriginDecoratorA，OriginDecoratorB，OriginDecoratorC

现在我们有了新的需求，需要为Origin类增加A,B功能。

动态代理：再生成一个子类来实现AB功能的组合，客户端代码获取对应功能的代理类可以解耦到配置文件中。
这样就可以不用修改客户端代码来实现新的功能了。
装饰器模式：不需要生成新的装饰器类，只需要在客户端代码中组合的OriginDecoratorA和OriginDecoratorB来或者AB功能。

*/