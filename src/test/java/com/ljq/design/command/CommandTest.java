package com.ljq.design.command;

import org.junit.Test;
/**
 * 
 * 适用场景：
	1. 命令的发送者和命令执行者有不同的生命周期。命令发送了并不是立即执行。
	2. 命令需要进行各种管理逻辑。
	3. 需要支持撤消\重做操作（这种状况的代码大家可以上网搜索下，有很多，这里不进行详细解读）。
 
 	命令模式：一次设定，统一执行。
	状态模式：
	    相当于If else if else；
	    设计路线：各个State类的内部实现(相当于If，else If内的条件)
	    执行时通过State调用Context方法来执行。
	职责链模式：
	    相当于Swich case
	    设计路线：客户设定，每个子类(case)的参数是下一个子类(case)。
	    使用时，向链的第一个子类的执行方法传递参数就可以。
	    
————————————————————————————————————
	     命令模式：将多个命令只提交给一个执行该命令的对象
 而职责链模式相反：只将一个请求提交给多个能执行该命令的对象

————————————————————————————————————
 状态模式与职责链模式的区别：
 	状态模式是让各个状态对象自己知道其下一个处理的对象是谁，即在编译时便设定好了的；
 	而职责链模式中的各个对象并不指定其下一个处理的对象到底是谁，只有在客户端才设定。
 
 * 
 *         CreateDate: 2017年12月1日
 */
public class CommandTest {

	@Test
	public void commandTest() {
		Receiver receiver = new Receiver();
		Command cmd = new MyCommand(receiver);
		Invoker invoker = new Invoker(cmd);
		invoker.action();
	}
}
