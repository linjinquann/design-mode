package com.ljq.design.facde;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ljq.design.dto.OrderInfoDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value="classpath:application.xml")
public class OrderBillFacdeTest {
	
	@Resource
	private OrderBillAdd orderBillAdd;
	@Resource
	private OrderBillOffer orderBillOffer;
	@Resource
	private IOrderBillFacde orderBillFacde;

	
	public OrderInfoDto getOrderInfo() {
		OrderInfoDto orderInfoDto = new OrderInfoDto();
		orderInfoDto.setOrderCode("orderCode");
		orderInfoDto.setOrderNo("orderNo");
		return orderInfoDto;
	}
	
	
	@Test
	public void add() {
		orderBillAdd.before(getOrderInfo());
		orderBillAdd.post();
		orderBillAdd.after();
		
		orderBillOffer.before(getOrderInfo());
		orderBillOffer.post();
		orderBillOffer.after();
	}
	
	@Test
	public void facde() {
		orderBillFacde.createOrderBill(getOrderInfo());
		orderBillFacde.offerOrderBill(getOrderInfo());
	}
	
}
