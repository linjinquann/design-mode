package com.ljq.design.bridge;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 桥接模式就是把事物和其具体实现分开，使他们可以各自独立的变化。
 * 桥接的用意是：将抽象化与实现化解耦，使得二者可以独立变化，
 * 像我们常用的JDBC桥DriverManager一样，JDBC进行连接数据库的时候，在各个数据库之间进行切换，基本不需要动太多的代码，甚至丝毫不用动，
 * 原因就是JDBC提供统一接口，每个数据库提供各自的实现，用一个叫做数据库驱动的程序来桥接就行了。
 * 
 *         CreateDate: 2017年11月30日
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value="classpath:application.xml")
public class BridegeTest {
	
	@Resource
	private MyBridge myBridge;
	@Resource
	private MySQLDriver mySQLDriver;
	@Resource
	private OracleDriver oracleDriver;
	
	@Test
	public void bridgeTest() {
		AbsBridge myBridge = new MyBridge();
		
		ISourceDriver mySQLDriver = new MySQLDriver();
		myBridge.setSourceDriver(mySQLDriver);
		myBridge.driver();

		ISourceDriver oracleDriver = new OracleDriver();
		myBridge.setSourceDriver(oracleDriver);
		myBridge.driver();
	}
	
	@Test
	public void sprinBridgeTest() {
		myBridge.setSourceDriver(mySQLDriver);
		myBridge.driver();

		myBridge.setSourceDriver(oracleDriver);
		myBridge.driver();
	}

}
