package com.ljq.design.template;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ljq.design.dto.OrderInfoDto;

/**
 * 准备一个抽象类，将部分逻辑以具体方法以及具体构造函数的形式实现，然后声明一些抽象方法来迫使子类实现剩余的逻辑。
 * 不同的子类可以以不同的方式实现这些抽象方法，从而对剩余的逻辑有不同的实现。这就是模板方法模式的用意。
 * 
 *         CreateDate: 2017年12月2日
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value="classpath:application.xml")
public class TemplateTest {
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	TemplateImpl templateInfImpl;
	
	public OrderInfoDto getOrderInfo() {
		OrderInfoDto orderInfoDto = new OrderInfoDto();
		orderInfoDto.setOrderCode("orderCode");
		orderInfoDto.setOrderNo("orderNo");
		return orderInfoDto;
	}
	
	@Test
	public void add() {
		Object object = templateInfImpl.cache(getOrderInfo());
		logger.info("result={}", object);
	}
	
}
