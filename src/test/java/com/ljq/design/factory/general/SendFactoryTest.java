package com.ljq.design.factory.general;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ljq.design.factory.ISender;
import com.ljq.design.factory.MailSender;
import com.ljq.design.factory.SmsSender;
import com.ljq.design.factory.abs.MailSenderFactory;
import com.ljq.design.factory.abs.SmsSenderFactory;
import com.ljq.design.factory.builder.SenderBuilder;

/**
 * 工厂模式适合：凡是出现了大量的产品需要创建，并且具有共同的接口时，
 * 可以通过工厂方法模式进行创建。
 * 在以上的三种模式中，第一种如果传入的字符串有误，不能正确创建对象，第三种相对于第二种，不需要实例化工厂类，所以，大多数情况下，我们会选用第三种——静态工厂方法模式。
 * 
 * @author 林进权
 *         CreateDate: 2017年11月29日
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value="classpath:application.xml")
public class SendFactoryTest {
	
	@Resource
	SendFactoryGeneral sendFactoryGeneral;
	@Resource
	SendFactoryStatic sendFactoryStatic;

	@Resource
	MailSenderFactory mailSenderFactory;
	
	@Resource
	SmsSenderFactory smsSenderFactory;
	@Resource
	SenderBuilder senderBuilder;
	
	/**
	 * 普通工厂
	 */
	@Test
	public void testSenderFactoryGeneral() {
		sendFactoryGeneral.getSender(SmsSender.class);
		sendFactoryGeneral.getSender(MailSender.class);
	}
	
	/**
	 * 静态工厂
	 */
	@Test
	public void testSenderFactoryStatic() {
		sendFactoryStatic.productMailSender();
		sendFactoryStatic.productSmsSender();
	}

	/**
	 * 抽象工厂，解藕，有新产品时，增加新产品和新工厂
	 * 方法说明：
	 *
	 * @param     设定文件 
	 * @return void    返回类型 
	 * @throws Exception
	 *
	 * @author 林进权
	 *         CreateDate: 2017年11月29日
	 */
	@Test
	public void testSenderFactory() {
		ISender sender = mailSenderFactory.produce();
		sender.Send();
		
		sender = smsSenderFactory.produce();
		sender.Send();
	}
	
	@Test
	public void testSenderBuilder() {
		senderBuilder.produceMailSender(10);
	}

}
